FROM alpine:3.20

ENV HOME=/home/app \
    APP_HOME=/home/app/docker-files-backup \
    \
    # python
    PYTHONPATH=${PYTHONPATH}:${PWD} \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_BREAK_SYSTEM_PACKAGES=1 \
    \
    # pip
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100

RUN apk --no-cache add zstd python3 py3-pip tini tar

COPY ./ $APP_HOME
WORKDIR $APP_HOME

RUN python3 -m pip install $APP_HOME

ENTRYPOINT ["tini", "--"]
CMD ["python3", "start.py"]
