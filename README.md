# docker-files-backup

This docker image provides functionality:

1. Compress directory using ZSTD
2. Upload compressed file to S3 storage
3. Backup running schedule (monthly, weekly, daily, hourly)

# SETTINGS

You can set some envs vars:
- SCHEDULE - can be monthly, weekly, daily (by default), hourly
- FOLDER_TO_BACKUP - folder which script will compress, default is `/backup`
- TIME_ZONE - timezone for datetime using in filenames, default is Europe/Tallinn
- COMPRESSION_LEVEL - level of LZMA compression, default is 10
- PREFIX - prefix for backup filename, default is empty

Also, please define settings for S3 storage:
- AWS_S3_REGION_NAME - default nl-ams
- AWS_S3_ENDPOINT_URL - default `https://s3.nl-ams.scw.cloud`
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_BUCKET_NAME

# HOW TO USE

You can use it directly in Docker Compose
```
services:
  backup-files:
    image: registry.gitlab.com/technocloud/docker-files-backup/docker-files-backup:latest
    volumes:
      - test_volume_first:/backup/test_volume_first
      - test_volume_second:/backup/test_volume_second
    environment:
      - AWS_ACCESS_KEY_ID=
      - AWS_SECRET_ACCESS_KEY=
      - AWS_BUCKET_NAME=
```
